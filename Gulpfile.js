var gulp = require('gulp'),
  sass = require('gulp-ruby-sass'),
  concat = require('gulp-concat'),
  react = require('gulp-react'),
  gulpIf = require('gulp-if'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  browserify = require('browserify'),
  watchify = require('watchify'),
  reactify = require('reactify'),
  uglify = require('gulp-uglify'),
  prefix = require('gulp-prefix'),
  minimist = require('minimist'),
  chalk = require('chalk'),
  plumber = require('gulp-plumber'),
  monitor = require('watch').createMonitor,
  express =require('express');

var paths = {
  style: './src/style',
  assets: './src/assets',
  scripts: './src/script',
  scriptEntryPoint: './src/script/init.js',
  build: './public'
}

var args = minimist(process.argv.slice(2));

var isProduction = !!args['production'];

function logError (error) {
  console.log(chalk.red(error));
}

function watchDir (path, callback) {
  monitor(path, {interval: 500}, function (watcher) {
    watcher.on('created', callback);
    watcher.on('changed', callback);
    watcher.on('removed', callback);
  });
}

function server () {
  express()
    .use(express.static(paths.build))
    .listen(3333);
}

function style () {
  gulp.src(paths.style + '/**.scss')
    .pipe(plumber())
    .pipe(sass({
      style: 'compressed'
    }))
    .pipe(prefix({
      browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Firefox >= 16']
    }))
    .pipe(concat('main.css'))
    .pipe(gulp.dest(paths.build))
    .on('error', logError);
}

function browserifyScripts (watch) {
  var opts = {
    entries: [paths.scriptEntryPoint],
    paths: [paths.scripts],
    fullPaths: true,
    cache: {}, 
    packageCache: {},
  }

  var bundler = browserify(opts);
  bundler.add(paths.scriptEntryPoint);
  bundler.transform(reactify);
  if (watch) bundler = watchify(bundler);

  var rebundle = function () {
    bundler
      .bundle()
      .on('error', logError)
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(gulpIf(isProduction, uglify()))
      .pipe(gulp.dest(paths.build));
  };

  bundler.on('update', rebundle);
  rebundle();
}

function assets () {
  gulp.src(paths.assets + '/**')
    .pipe(gulp.dest(paths.build));
}

function build () {
  setTimeout(function () {
    browserifyScripts(false);
    style();
    assets();
  }, 500)
}

function watch () {
  browserifyScripts(true);
  watchDir(paths.style, style);
  watchDir(paths.assets, assets);
}

gulp.task('build', build);
gulp.task('watch', ['build'], watch);
gulp.task('server', ['watch'], server);