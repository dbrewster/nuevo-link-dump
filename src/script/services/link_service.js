var makeService = require('lib/make_service'),
  Link = require('models/link');

var links = [];

var LinkService = makeService({

  loadLinks: function () {
    new Parse.Query(Link)
      .descending('createdAt')
      .find()
      .then(function (results) {
        links = results;
      })
      .then(this.triggerChange)
  },

  getLinks: function () {
    return links;
  },

  createLink: function (attrs) {
    new Link(attrs).save()
      .then(this.loadLinks)
  }

});

module.exports = LinkService;
