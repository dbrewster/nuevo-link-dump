var React = require('lib/react'),
  LinkService = require('services/link_service'),
  LinkStub = require('components/link_stub');

var LinkList = React.createClass({
  getInitialState: function () {
    return {
      links: LinkService.getLinks()
    }
  },

  componentDidMount: function () {
    LinkService.loadLinks();
    LinkService.on('change', this.updateLinks);
  },

  updateLinks: function () {
    this.setState({
      links: LinkService.getLinks()
    });
  },

  render: function () {
    return (
      <div className="link-list">
        { this.state.links.map(function (link) {
          return (<LinkStub model={link} />)
        }) }
      </div>
    )
  }
});

module.exports = LinkList;
