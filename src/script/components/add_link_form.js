var React = require('lib/react'),
  LinkService = require('services/link_service'),
  UserService = require('services/user_service');

var AddLinkForm = React.createClass({

  getInitialState: function () {
    return {
      value: "", 
      valid: true
    };
  },

  onKeyUp: function (e) {
    if (e.keyCode === 13) {
      this.submit();
    }
  },

  onChange: function (e) {
    var isValid = !!this.getFormattedLink() || this.state.value.length < 3;

    this.setState({
      value: e.target.value,
      valid: isValid
    });
  },

  submit: function () {
    var link = this.getFormattedLink();
    var user = UserService.getUser();

    if (!link) {
      return this.setState({valid: false});
    }

    LinkService.createLink({
      text: link,
      username: user.get('name'),
      avatar: user.get('avatar')
    });
  
    this.setState({value: ""});
  },

  getFormattedLink: function () {
    var val = this.state.value;
    val = val.replace(/^\s+/, '').replace('\s+$', '');

    if (val.match(/\s/g)) {
      return false;
    }

    var protocol = val.split('://').shift();
    if (protocol !== val && protocol !== 'http' && protocol !== 'https') {
      return false;
    }

    if (protocol === val) {
      val = 'http://' + val;
    }

    return val;
  },
 
  render: function () {
    return (
      <div className={React.addons.classSet({
        "add-link-form": true,
        "is-active": this.props.active,
        "is-invalid": !this.state.valid
      })}>
        <input onChange={this.onChange} 
          onKeyUp={this.onKeyUp} 
          type="text" placeholder="url" 
          value={this.state.value} />
      </div>
    );
  }

});

module.exports = AddLinkForm;

