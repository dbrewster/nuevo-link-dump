var config = {
  parseApplicationId: '<APPLICATION ID>',
  parseJavascriptKey: 'YOUR PARSE JAVASCRIPT KEY'
};

if (config.parseJavascriptKey == '' || config.parseApplicationId == '') {
  throw new Error('Please specify your parse javascript key and application id in config.js');
}

module.exports = config;
