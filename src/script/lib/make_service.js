var EventEmitter = require('wolfy87-eventemitter');

module.exports = function (serviceObj) {
  var base = Object.create(EventEmitter.prototype);

  for (key in serviceObj) {
    var val = serviceObj[key];

    if (typeof val === 'function') { 
      val = val.bind(base);
    }

    base[key] = val;
  }

  base.triggerChange = function () {
    this.trigger('change');
  }.bind(base);

  return base;
}